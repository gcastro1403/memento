package memento;

import java.util.ArrayList;
import java.util.List;

public class Carateker {
	
	
	private List<Memento> states = new ArrayList<>();
	
	
	public void setMemento (Memento memento) {
		
		states.add(memento);
		
	}
	
	
	public Memento getMemento(int index) {
		
		return states.get(index);
		
	}
}
