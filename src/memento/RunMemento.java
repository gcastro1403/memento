package memento;

public class RunMemento {

	public static void main(String[] args) {
		
		Carateker carateker = new Carateker();
		Originator memento = new Originator("Jose", "Perez");
		
		memento.setApellido(memento.getApellido() + " Lopez");
		System.out.println(memento.getNombre() +" "+ memento.getApellido());
		carateker.setMemento(memento.createMemento());
		
		memento.setNombre(memento.getNombre() + " Juan");
		System.out.println(memento.getNombre() +" "+ memento.getApellido());
		carateker.setMemento(memento.createMemento());
		
		Memento memento1 = carateker.getMemento(0);
		Memento memento2 = carateker.getMemento(1);
		
		System.out.println(memento1);
		System.out.println(memento2);
		
		
		//output console
		
		//Jose Perez Lopez
		//Jose Juan Perez Lopez
		//Jose Perez Lopez
		//Jose Juan Perez Lopez
		
		
		
		
	}

}
